// @dart=2.9
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stockflow/preference/preferenceStore.dart';
import 'package:stockflow/screen/history.dart';
import 'package:stockflow/screen/splash.dart';

import 'screen/login.dart';

Future<void> main() async {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     return MaterialApp(
       debugShowCheckedModeBanner: false,
    title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const Splash(),
    );
    /*return FutureBuilder(
        future: SharedPreferences.getInstance(),
        builder:
            (BuildContext context, AsyncSnapshot<SharedPreferences> prefs) {
          var x = prefs.data;
          if (prefs.hasData) {
            //if(x!.getString(PrefStore.token)!=null){
            return const MaterialApp(home: History());
            // }else{
            //   return const MaterialApp(home: Login());
            // }
          }
          return const MaterialApp(home: Login());
        });*/
  }
}
