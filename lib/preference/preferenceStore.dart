

import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:stockflow/api/loginApi.dart';

class PrefStore {
   static String token = "auth_token";
   static String usertype = "type";
   static String islogin = "islogin";
   static String user = "user";

  Future<void> setLogin(bool b) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(islogin, b);
  }
  Future<bool> getIsLogin() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
   bool b= pref.getBool(islogin);
    return b;
  }


  Future<void> setAuthToken(String tkn) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(token, tkn);
  }

   Future<String> getAuthToken() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    String auth_token;
    print(pref.getString(token));
    auth_token = pref.getString(token);
    return auth_token;
  }
    /*doSomething() async {

   }*/
   static doSomething()async {
     final SharedPreferences pref = await SharedPreferences.getInstance();
     String auth_token;
     print(pref.getString(token));
     auth_token = pref.getString(token);
     return auth_token;
   }
  Future<void> setUser(UserData tkn) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(user, jsonEncode(tkn.toString()));
  }
  Future<UserModel> getUser() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    String data;
    data = (pref.getString(user))!;
    UserModel model = UserModel.fromJson(jsonDecode(pref.getString(user)));
    return model;
  }

  static void remove() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();


  }


}
