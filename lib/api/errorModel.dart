
class ErrorModel {
  final int status;
  final String message;
  //final Results results;

  ErrorModel({
    required this.status,
    required this.message,
    //required this.results,
  });

  factory ErrorModel.fromJson(Map<String, dynamic> json) {
    return ErrorModel(
      status: json['status'],
      message: json['message'],
      //results: Results.fromJson(json['results']),
    );
  }
}