import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'collection.dart';

loginUser(String number, String pass) async {
  Map data = {'email': number, 'password': pass};
  try {
    http
        .post(Uri.parse(collection.endpoint_login), body: data)
        .then((response) {
      if (response.statusCode == 200) {
        UserModel model = UserModel.fromJson(jsonDecode(response.body));
        print("------>>>>>: ${model.status}");
        print("------>>>>>: ${model.results}");
        Results results = model.results;
        print("------>>>>>: ${results.access_token}");
        UserData data = results.user;
        print("------>>>>>: ${data.id}");
        return UserModel.fromJson(jsonDecode(response.body));
      } else {
        throw Exception('Login Failed');
      }
    });
  } catch (Exc) {
    print(Exc);
    rethrow;
  }
}

class UserModel {
  final int status;
  final String message;
  final Results results;

  UserModel({
    required this.status,
    required this.message,
    required this.results,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      status: json['status'],
      message: json['message'],
      results: Results.fromJson(json['results']),
    );
  }
}

class Results {
  String access_token;
  String token_type;
  int expires_in;
  UserData user;

  Results({
    required this.access_token,
    required this.token_type,
    required this.expires_in,
    required this.user,
  });

  factory Results.fromJson(Map<String, dynamic> json) {
    return Results(
      access_token: json['access_token'],
      token_type: json['token_type'],
      expires_in: json['expires_in'],
      user: UserData.fromJson(json['user']),
    );
  }
}

class UserData {
  int id;
  String fname;
  String lname;
  String mobile;
  String profile_pic;
  String email;
  String email_verified_at;
  String user_type;
  String status;
  String device_token;
  String device_name;
  String created_at;
  String updated_at;
  String deleted_at;

  UserData({
    required this.id,
    required this.fname,
    required this.lname,
    required this.mobile,
    required this.profile_pic,
    required this.email,
    required this.email_verified_at,
    required this.user_type,
    required this.status,
    required this.device_token,
    required this.device_name,
    required this.created_at,
    required this.updated_at,
    required this.deleted_at,
  });

  factory UserData.fromJson(Map<String, dynamic> json) {
    return UserData(
      id: json['id'],
      fname: json['fname'],
      lname: json['lname'],
      mobile: json['mobile'],
      profile_pic: json['profile_pic'],
      email: json['email'],
      email_verified_at: json['email_verified_at'],
      user_type: json['user_type'],
      status: json['status'],
      device_token: json['device_token'],
      device_name: json['device_name'],
      created_at: json['created_at'],
      updated_at: json['updated_at'],
      deleted_at: json['deleted_at'],
    );
  }
}
