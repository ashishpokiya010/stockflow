import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stockflow/dialog/add_dialog.dart';
import 'package:stockflow/dialog/logout_dialog.dart';
import 'package:stockflow/dialog/product_dialog.dart';
import 'package:stockflow/model/historydata.dart';
import 'package:stockflow/model/product.dart';
import 'package:stockflow/preference/preferenceStore.dart';
import 'package:stockflow/utils/constants.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:stockflow/api/collection.dart';
import 'package:intl/intl.dart';

import 'login.dart';

class History extends StatefulWidget {
  const History({Key? key}) : super(key: key);

  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  late SharedPreferences pref;
  late int tappedIndex;
  late String userType = '';
  late String token;
  late Product selected;
  late String pId = '';
  late String pname = '';
  List<Product> products = [];
  List<HistoryData> productHistory = [];
  List<HistoryData> recentHistory = [];
  ScrollController controller = ScrollController();

  bool isProgress = false;
  bool isRecentProgress = true;
  bool isrevers = true;
  bool isRecentRevers = true;

  @override
  void initState() {
    super.initState();
    tappedIndex = -1;
    getUserStatus();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: appbarModel(),
        body: Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              productHistory.isEmpty
                  ? /*userType == '1'
                      ? Expanded(
                          child: Center(
                          child: isProgress
                              ? CircularProgressIndicator()
                              : Container(
                                  margin: EdgeInsets.all(20),
                                  decoration: BoxDecoration(
                                      color: Constants.yellow_white,
                                      borderRadius: BorderRadius.circular(15)),
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        width: double.infinity,
                                        padding: EdgeInsets.all(22),
                                        margin: EdgeInsets.only(bottom: 10),
                                        decoration: BoxDecoration(
                                            color: Constants.yellow,
                                            borderRadius:
                                                BorderRadius.circular(15)),
                                        child: Text(
                                          Constants.recent_update,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 20,
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                      Expanded(
                                              child:isRecentProgress? Center(child: CircularProgressIndicator())
                                                  : SingleChildScrollView(
                                                physics:
                                                AlwaysScrollableScrollPhysics(),
                                                //reverse: isRecentRevers,
                                                child: ListView.builder(
                                                    shrinkWrap: true,
                                                    padding: EdgeInsets.zero,
                                                    physics:
                                                    const NeverScrollableScrollPhysics(),
                                                    itemCount: recentHistory.length,
                                                    itemBuilder:
                                                        (BuildContext context,
                                                        int index) {
                                                      return Container(
                                                        child: ListTile(
                                                            title: recentListView(
                                                                recentHistory[index]),
                                                            onTap: () {}),
                                                      );
                                                    }),
                                              )
                                      )
                                    ],
                                  ),
                                )
                            ,
                        )
              ):*/ Expanded(
                          child: Center(
                              child: isProgress
                                  ? CircularProgressIndicator()
                                  : const Text(
                                      Constants.msg_no_product_history,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 17,
                                          fontWeight: FontWeight.w400),
                                    )))
                  : Expanded(
                      child: productHistory.length > 0
                          ? SingleChildScrollView(
                              physics: AlwaysScrollableScrollPhysics(),
                              reverse: isrevers,
                              child: ListView.builder(
                                  shrinkWrap: true,
                                  controller: controller,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: productHistory.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Container(
                                      child: ListTile(
                                          title: cardView(
                                              productHistory[index], index),
                                          //title: selectedCardView(index),
                                          onTap: () {
                                            setState(() {
                                              /*if(userType=='1') {
                                                if (tappedIndex == index) {
                                                  tappedIndex = -1;
                                                } else {
                                                  tappedIndex = index;
                                                }
                                              }*/
                                            });
                                          }),
                                    );
                                  }),
                            )
                          : Container(),
                    ),
              bottomView(),
            ],
          ),
        ),
      ),
    );
  }

  appbarModel() {
    return AppBar(
      toolbarHeight: 75,
      backgroundColor: Constants.blue,
      title: Container(
        height: 35,
        width: 130,
        decoration: const BoxDecoration(
            image: DecorationImage(
          image: AssetImage('assets/icons/label_icon.png'),
          fit: BoxFit.fill,
        )),
      ),
      actions: <Widget>[
        Padding(
            padding: const EdgeInsets.only(right: 20.0),
            child: GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return const LogoutDialog();
                    });
              },
              child: const Icon(
                Icons.account_circle,
                size: 26.0,
              ),
            )),
      ],
    );
  }

  bottomView() {
    return Container(
        padding: const EdgeInsets.all(5.0),
        margin: const EdgeInsets.all(5.0),
        child: Row(
          children: <Widget>[
            Expanded(
              child: GestureDetector(
                onTap: () {
                  if (products.length > 0) {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return ProductDialog(products);
                        }).then((value) {
                      print('dialog---->>>>$value');
                      if (value != null) {
                        setState(() {
                          selected = value;
                          pId = selected.id.toString();
                          pname = selected.product_name.toString();
                          isProgress = true;
                        });
                        getProductHistory(pId);
                      }
                    });
                  } else {
                    showSnackBar(Constants.msg_no_product_available);
                  }
                },
                child: Container(
                  height: 55,
                  width: double.infinity,
                  margin: const EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(30)),
                  child: Container(
                      child: Row(children: <Widget>[
                    Expanded(
                      child: Container(
                        padding:
                            const EdgeInsets.fromLTRB(20.0, 0.0, 10.0, 0.0),
                        child: Text(
                          pname.isEmpty ? Constants.select_product : pname,
                          textAlign: TextAlign.left,
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 15,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Container(
                        margin: const EdgeInsets.fromLTRB(5.0, 0.0, 20.0, 0.0),
                        child: const IconTheme(
                          data: IconThemeData(color: Colors.black),
                          child: Icon(Icons.arrow_forward),
                        ),
                      ),
                    ),
                  ])),
                ),
              ),
            ),
            //(tappedIndex != -1) ? checkView() : plusMinusView(),
            userType == '1' ? Container() : plusMinusView(),
          ],
        ));
  }

  Widget cardView(HistoryData history, int index) {
    print('cardview--->>$index');
    int tt = history.update_type == Constants.Minus
        ? 1
        : 0; //((index % 2) == 0) ? 0 : 1;
    //Color changes by selected view;
    Color textColor = (tappedIndex == index) ? Colors.white : Colors.black;
    Color dateColor = (tappedIndex == index) ? Colors.white : Colors.grey;
    Color iconColor = (tappedIndex == index) ? Constants.blue : Colors.white;
    Color iconShapeColor =
        (tappedIndex == index) ? Colors.white : Constants.blue;
    /*return (tappedIndex == index)
        ? selectedCardView(history, index)
        : Container(
            padding: tt == 0
                ? const EdgeInsets.fromLTRB(10.0, 10.0, 45.0, 5.0)
                : const EdgeInsets.fromLTRB(45.0, 10.0, 10.0, 5.0),
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              color: tappedIndex == index ? Constants.blue : Colors.grey[300],
              elevation: 2,
              child: Container(
                padding: const EdgeInsets.fromLTRB(10.0, 5.0, 10, 5.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              padding: const EdgeInsets.all(5.0),
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    SizedBox(
                                      width: double.infinity,
                                      child: Text(
                                        Constants.product,
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          color: textColor,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: double.infinity,
                                      child: Text(
                                        history.product.product_name,
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            color: textColor,
                                            fontSize: 15,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    )
                                  ])),
                        ),
                        Expanded(
                          child: Container(
                              padding: EdgeInsets.all(5.0),
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    SizedBox(
                                      width: double.infinity,
                                      child: Text(
                                        tt == 0
                                            ? Constants.quantity_added
                                            : Constants.quantity_remove,
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                          color: textColor,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: double.infinity,
                                      child: Text(
                                        '${history.update_quantity}',
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                            color: textColor,
                                            fontSize: 15,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ])),
                        ),
                      ],
                    ),
                    Container(
                      padding: const EdgeInsets.all(5.0),
                      margin: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                      width: double.infinity,
                      child: Text(
                        Constants.remarks,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: textColor,
                          fontSize: 14,
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
                      margin: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                      width: double.infinity,
                      child: Text(
                        history.remark,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: textColor,
                          fontSize: 14,
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                      margin: const EdgeInsets.fromLTRB(5.0, 15.0, 5.0, 5.0),
                      width: double.infinity,
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              //'01 Dec 2021,10:05 PM',
                              getTimeFormate(history.created_at),
                              style: TextStyle(
                                  color: dateColor,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w300),
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomRight,
                            child: Container(
                              height: 30,
                              width: 30,
                              margin:
                                  const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                              decoration: BoxDecoration(
                                  color: iconShapeColor,
                                  borderRadius: BorderRadius.circular(25)),
                              child: IconTheme(
                                data: IconThemeData(color: iconColor),
                                child: tt == 0
                                    ? const Icon(Icons.add)
                                    : const Icon(Icons.remove),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );*/
    print('usertype---->>>${userType}');
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            padding: tt == 0
                ? const EdgeInsets.fromLTRB(0.0, 10.0, 45.0, 5.0)
                : const EdgeInsets.fromLTRB(45.0, 10.0, 0.0, 5.0),
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              color: tappedIndex == index ? Constants.blue : Colors.grey[300],
              elevation: 2,
              child: Container(
                padding: const EdgeInsets.fromLTRB(10.0, 5.0, 10, 5.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              padding: const EdgeInsets.all(5.0),
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    SizedBox(
                                      width: double.infinity,
                                      child: Text(
                                        Constants.product,
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          color: textColor,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: double.infinity,
                                      child: Text(
                                        history.product.product_name,
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            color: textColor,
                                            fontSize: 15,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    )
                                  ])),
                        ),
                        Expanded(
                          child: Container(
                              padding: EdgeInsets.all(5.0),
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    SizedBox(
                                      width: double.infinity,
                                      child: Text(
                                        Constants.quantity,
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                          color: textColor,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: double.infinity,
                                      child: Text(
                                        '${history.update_quantity}',
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                            color: textColor,
                                            fontSize: 15,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ])),
                        ),
                      ],
                    ),
                    Container(
                      padding: const EdgeInsets.all(5.0),
                      margin: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                      width: double.infinity,
                      child: Text(
                        Constants.remarks,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: textColor,
                          fontSize: 14,
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
                      margin: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                      width: double.infinity,
                      child: Text(
                        history.remark,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: textColor,
                          fontSize: 14,
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                      margin: const EdgeInsets.fromLTRB(5.0, 15.0, 5.0, 5.0),
                      width: double.infinity,
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            /*child: Text(
                              //'01 Dec 2021,10:05 PM',
                              getTimeFormate(history.updated_at),
                              style: TextStyle(
                                  color: dateColor,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w300),
                            ),*/
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Visibility(
                                    visible: index == 0 ? false : true,
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Text(
                                              'Update by  ',
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                color: textColor,
                                                fontSize: 14,
                                              ),
                                            ),
                                            Text(
                                              history.users.mobile,
                                              style: TextStyle(
                                                  color: dateColor,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w300),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                      ],
                                    )),
                                Text(
                                  //'01 Dec 2021,10:05 PM',
                                  getTimeFormate(history.updated_at),
                                  style: TextStyle(
                                      color: dateColor,
                                      fontSize: 12,
                                      fontWeight: FontWeight.w300),
                                ),
                              ],
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomRight,
                            child: Container(
                              height: 30,
                              width: 30,
                              margin:
                                  const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                              decoration: BoxDecoration(
                                  color: iconShapeColor,
                                  borderRadius: BorderRadius.circular(25)),
                              child: IconTheme(
                                data: IconThemeData(color: iconColor),
                                child: tt == 0
                                    ? const Icon(Icons.add)
                                    : const Icon(Icons.remove),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          /*userType == '1'
              ? ((productHistory.length - 1) == index)
                  ? selectedCardView(history, index)
                  : Container()
              : Container()*/
          ((productHistory.length - 1) == index)
              ? selectedCardView(history, index)
              : Container()
        ],
      ),
    );
  }

  Widget recentListView(HistoryData data) {
    return Container(
      margin: const EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        color: Constants.blue,
        elevation: 2,
        child: Container(
          padding: const EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                        padding: const EdgeInsets.all(5.0),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              SizedBox(
                                width: double.infinity,
                                child: Text(
                                  Constants.product,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 14,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: double.infinity,
                                child: Text(
                                  data.product.product_name,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500),
                                ),
                              )
                            ])),
                  ),
                  Expanded(
                    child: Container(
                        padding: EdgeInsets.all(5.0),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              SizedBox(
                                width: double.infinity,
                                child: Text(
                                  Constants.total_stock,
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: double.infinity,
                                child: Text(
                                  '${data.product.product_quantity}',
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ])),
                  ),
                ],
              ),
              Container(
                height: 5,
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
                margin: const EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 5.0),
                width: double.infinity,
                  child:Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text(
                                    'Update by  ',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 14,
                                    ),
                                  ),
                                  Text(
                                    data.users.mobile,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w300),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 5,
                              ),
                            ],
                          ),
                      Text(
                        //'01 Dec 2021,10:05 PM',
                        getTimeFormate(data.updated_at),
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                            fontWeight: FontWeight.w300),
                      ),
                    ],
                  )
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget selectedCardView(HistoryData history, int index) {
    int tt =
        history.update_type == 'Minus' ? 1 : 0; //((index % 2) == 0) ? 0 : 1;
    //Color changes by selected view;
    Color textColor = Colors.white;
    Color dateColor = Colors.white;
    Color iconColor = Constants.blue;
    Color iconShapeColor = Colors.white;
    Color cardColor = Constants.blue;
    return Container(
      /* padding: tt == 0
          ? const EdgeInsets.fromLTRB(10.0, 10.0, 45.0, 5.0)
          : const EdgeInsets.fromLTRB(45.0, 10.0, 10.0, 5.0),*/
      padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 5.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        color: cardColor,
        elevation: 2,
        child: Container(
          padding: const EdgeInsets.fromLTRB(10.0, 5.0, 10, 5.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                        padding: const EdgeInsets.all(5.0),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              SizedBox(
                                width: double.infinity,
                                child: Text(
                                  Constants.product,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    color: textColor,
                                    fontSize: 14,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: double.infinity,
                                child: Text(
                                  history.product.product_name,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: textColor,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w500),
                                ),
                              )
                            ])),
                  ),
                  Expanded(
                    child: Container(
                        padding: EdgeInsets.all(5.0),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              SizedBox(
                                width: double.infinity,
                                child: Text(
                                  Constants.total_stock,
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                    color: textColor,
                                    fontSize: 14,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: double.infinity,
                                child: Text(
                                  '${history.product.product_quantity}',
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                      color: textColor,
                                      fontSize: 35,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ])),
                  ),
                ],
              ),
              Container(
                height: 40,
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
                margin: const EdgeInsets.fromLTRB(5.0, 15.0, 5.0, 5.0),
                width: double.infinity,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        getTimeFormate(history.product.updated_at),
                        style: TextStyle(
                            color: dateColor,
                            fontSize: 12,
                            fontWeight: FontWeight.w300),
                      ),
                    ),
                    Container(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget plusMinusView() {
    return Container(
        height: 55,
        width: 120,
        margin: const EdgeInsets.fromLTRB(5.0, 0.0, 10.0, 0.0),
        decoration: BoxDecoration(
            color: Colors.transparent, borderRadius: BorderRadius.circular(15)),
        child: Row(children: <Widget>[
          GestureDetector(
              onTap: () {
                if (pId.isEmpty) {
                  showSnackBar(Constants.msg_product_selection);
                } else {
                  print('***** $pId');
                  showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        return AddDialog(type: Constants.add, productId: pId);
                      }).then((value) {
                    if (value != null) {
                      setState(() {
                        pId = value;
                        isProgress = true;
                      });
                      getProductHistory(pId);
                    }
                  });
                }
              },
              child: Container(
                height: 55,
                width: 55,
                margin: const EdgeInsets.fromLTRB(0.0, 0.0, 5.0, 0.0),
                decoration: BoxDecoration(
                    color: Constants.blue,
                    borderRadius: BorderRadius.circular(100)),
                child: const IconTheme(
                  data: IconThemeData(color: Colors.white),
                  child: Icon(Icons.add),
                ),
              )),
          GestureDetector(
            onTap: () {
              if (pId.isEmpty) {
                showSnackBar(Constants.msg_product_selection);
              } else {
                print('***** $pId');
                showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return AddDialog(type: Constants.remove, productId: pId);
                    }).then((value) {
                  if (value != null) {
                    setState(() {
                      pId = value;
                      isProgress = true;
                    });
                    getProductHistory(pId);
                  }
                });
              }
            },
            child: Container(
              height: 55,
              width: 55,
              margin: const EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
              decoration: BoxDecoration(
                  color: Constants.blue,
                  borderRadius: BorderRadius.circular(100)),
              child: const IconTheme(
                data: IconThemeData(color: Colors.white),
                child: Icon(Icons.remove),
              ),
            ),
          ),
        ]));
  }

  void showSnackBar(String s) {
    Fluttertoast.showToast(
        msg: s,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1);
  }

  Widget checkView() {
    return Container(
      height: 55,
      width: 120,
      padding: const EdgeInsets.all(10.0),
      margin: const EdgeInsets.fromLTRB(5.0, 0.0, 10.0, 0.0),
      decoration: BoxDecoration(
          color: Constants.blue, borderRadius: BorderRadius.circular(15)),
      child: GestureDetector(
        onTap: () {
          showDialog(
              context: context,
              barrierDismissible: false,
              builder: (BuildContext context) {
                return const AddDialog();
              });
        },
        child: Container(
          child: const Center(
            child: Text(
              Constants.check,
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                  fontWeight: FontWeight.w400),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> getUserStatus() async {
    pref = await SharedPreferences.getInstance();
    if (pref.getString(PrefStore.token) != null) {
      token = pref.getString(PrefStore.token);
      setState(() {
        userType = pref.getString(PrefStore.usertype);
      });
      print("history------>>>>>: ${token}----- $userType");
      if (userType == '1') {
        getProduct('1');
        //getRecentProduct();
      } else if (userType == '2') {
        getProduct('2');
      }
    }
  }

  void getProduct(String type) {
    String apiEndPoint = '';
    if (type == '1') {
      apiEndPoint = collection.endpoint_product_list_admin;
    } else {
      apiEndPoint = collection.endpoint_product_list;
    }
    try {
      http.post(Uri.parse(apiEndPoint), headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }).then((response) {
        print("response------>>>>>: ${response.statusCode}");
        if (response.statusCode == 201) {
          print("product------>>>>>: ${response.body}");
          ProductModel model = ProductModel.fromJson(jsonDecode(response.body));
          setState(() {
            products = model.results;
          });
          print("product------>>>>>: ${products.length}");
        } else if (response.statusCode == 401) {
          logout();
        } else {
          throw Exception('product Failed');
        }
      });
    } catch (Exc) {
      print(Exc);
      rethrow;
    }
  }

  void getRecentProduct() {
    String apiEndPoint = collection.endpoint_product_history;
    Map data = {'product_id': '0'};
    try {
      http
          .post(Uri.parse(apiEndPoint),
              headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer $token',
              },
              body: data)
          .then((response) {
        isRecentProgress = false;
        print("History------>>>>>: ${response.statusCode}");
        if (response.statusCode == 201) {
          print("History------>>>>>: ${response.body}");
          HistoryModel model = HistoryModel.fromJson(jsonDecode(response.body));
          print("History------>>>>>: ${model.results.length}");
          setState(() {
            recentHistory = model.results;
            isRecentRevers = true;
          });
        } else if (response.statusCode == 401) {
          logout();
        } else {
          throw Exception('product History Failed');
        }
      });
    } catch (Exc) {
      isRecentProgress = false;
      print(Exc);
      rethrow;
    }
  }

  logout() {
    PrefStore.remove();
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => const Login()),
        ModalRoute.withName("/login"));
  }

  void getProductHistory(value) {
    String apiEndPoint = collection.endpoint_product_history;
    Map data = {'product_id': value};
    try {
      http
          .post(Uri.parse(apiEndPoint),
              headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer $token',
              },
              body: data)
          .then((response) {
        isProgress = false;
        print("History------>>>>>: ${response.statusCode}");
        if (response.statusCode == 201) {
          print("History------>>>>>: ${response.body}");
          HistoryModel model = HistoryModel.fromJson(jsonDecode(response.body));
          print("History------>>>>>: ${model.results.length}");
          setState(() {
            productHistory = model.results;
            isrevers = true;
          });
        } else if (response.statusCode == 401) {
          logout();
        } else {
          throw Exception('product History Failed');
        }
      });
    } catch (Exc) {
      isProgress = false;
      print(Exc);
      rethrow;
    }
  }

  String getTimeFormate(String created_at) {
    //date = '2021-01-26T03:17:00.000000Z';
    DateTime parseDate =
        //DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(created_at);
        DateFormat("yyyy-MM-dd HH:mm:ss").parse(created_at);
    var inputDate = DateTime.parse(parseDate.toString());
    var outputFormat = DateFormat('dd MMM yyyy, hh:mm a');
    var outputDate = outputFormat.format(inputDate);
    print(outputDate);
    return outputDate;
  }
}
