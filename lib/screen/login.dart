import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stockflow/api/errorModel.dart';
import 'package:stockflow/api/loginApi.dart';
import 'package:stockflow/preference/preferenceStore.dart';
import 'package:stockflow/screen/history.dart';
import 'package:stockflow/utils/Cuvedshape.dart';
import 'package:stockflow/utils/constants.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'order.dart';
import 'package:stockflow/api/collection.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  late SharedPreferences pref;
  final _numberController = TextEditingController();
  final _passwordController = TextEditingController();
  String? _numberError = '';
  String? _passError = '';
  bool isProgress = false;

  bool isLogin = false;
  bool isValidMobile = true;
  bool isValidPass = true;

  @override
  void initState() {
    super.initState();
    //checkIsLogin();
  }

  @override
  Widget build(BuildContext context) {
    //final height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SingleChildScrollView(
          child: Stack(children: <Widget>[
            Column(
              children: <Widget>[
               /* ClipPath(
                  clipper: Cuvedshape(),
                  child: Container(
                    width: double.infinity,
                    height: 280.0,
                    color: Constants.blue,
                    alignment: Alignment.center,
                    child: const Text(Constants.appName,
                        style: TextStyle(
                            fontSize: 25,
                            color: Colors.white,
                            fontWeight: FontWeight.w500)),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 280.0,
                  color: Constants.blue,
                  alignment: Alignment.center,
                  child: const Text(Constants.appName,
                      style: TextStyle(
                          fontSize: 25,
                          color: Colors.white,
                          fontWeight: FontWeight.w500)),
                ),*/
                Container(
                    height: 290,
                    decoration: BoxDecoration(
                        color: Constants.blue,
                        image: DecorationImage(
                          image: AssetImage('assets/images/stockflow_logo.png'),
                          fit: BoxFit.contain,
                        ))),
                Container(
                  padding: const EdgeInsets.fromLTRB(30.0, 50.0, 30.0, 30.0),
                  alignment: Alignment.topCenter,
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        const Text(Constants.login_disc,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w400)),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 55, bottom: 5),
                          child: numberField(),
                        ),
                        Padding(
                            padding: const EdgeInsets.only(
                                left: 15.0, right: 15.0, top: 20, bottom: 5),
                            child: passwordField()),
                        isProgress
                            ? const Padding(
                            padding: EdgeInsets.only(
                                left: 15.0, right: 15.0, top: 20, bottom: 5),
                            child: CircularProgressIndicator())
                            : Padding(
                            padding: const EdgeInsets.only(
                                left: 15.0, right: 15.0, top: 20, bottom: 5),
                            child: button())
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ])),
    );
  }

  Widget numberField() {
    double customHeight = 50.0;

    return Container(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 0.3,
                      color: Colors.grey,
                    ),
                  ),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          ConstrainedBox(
                            constraints:
                            const BoxConstraints(minWidth: 55, maxWidth: 60),
                            child: IntrinsicWidth(
                                child: TextFormField(
                                  initialValue: '+91',
                                  decoration: const InputDecoration(
                                    isDense: true,
                                    contentPadding:
                                    EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.transparent, width: 0.0),
                                    ),
                                    border: InputBorder.none,
                                    hintText: '+91',
                                  ),
                                )),
                          ),
                          Container(
                            height: customHeight / 2,
                            width: 0.5,
                            decoration: BoxDecoration(
                                color: Colors.grey,
                                borderRadius: BorderRadius.circular(0)),
                          ),
                          Expanded(
                            child: TextFormField(
                              controller: _numberController,
                              onChanged: (inputValue) {
                                setState(() {
                                  if (inputValue.isEmpty) {
                                    isValidMobile = false;
                                  } else {
                                    isValidMobile = true;
                                  }
                                });
                              },
                              keyboardType: TextInputType.number,
                              decoration: const InputDecoration(
                                hintText: Constants.mobile_number,
                                //errorText: isValidMobile ? null : _numberError,
                                contentPadding: EdgeInsets.all(15.0),
                                isDense: true,
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.transparent, width: 0.0),
                                ),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  )),
              Visibility(
                  visible: isValidMobile ? false : true,
                  child: Container(
                    padding: const EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
                    child: Text(
                      '$_numberError',
                      textAlign: TextAlign.start,
                      style: const TextStyle(
                          color: Colors.red,
                          fontSize: 12,
                          fontWeight: FontWeight.w400),
                    ),
                  )
              ),
            ]));
  }

  Widget passwordField() {
    double customHeight = 50.0;
    return Container(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 0.3,
                      color: Colors.grey,
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          ConstrainedBox(
                            constraints:
                            const BoxConstraints(minWidth: 55, maxWidth: 60),
                            child: IntrinsicWidth(
                                child: TextFormField(
                                  enabled: false,
                                  initialValue: Constants.pin,
                                  decoration: const InputDecoration(
                                    isDense: true,
                                    contentPadding:
                                    EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.transparent, width: 0.0),
                                    ),
                                    border: InputBorder.none,
                                    hintText: Constants.pin,
                                  ),
                                )),
                          ),
                          Container(
                            height: customHeight / 2,
                            width: 0.5,
                            decoration: BoxDecoration(
                                color: Colors.grey,
                                borderRadius: BorderRadius.circular(0)),
                          ),
                          Expanded(
                            // wrap your Column in Expanded
                            child: TextFormField(
                              obscuringCharacter: "*",
                              obscureText: true,
                              controller: _passwordController,
                              onChanged: (inputValue) {
                                setState(() {
                                  if (inputValue.isEmpty) {
                                    isValidPass = false;
                                  } else {
                                    isValidPass = true;
                                  }
                                });
                              },
                              keyboardType: TextInputType.number,
                              decoration: const InputDecoration(
                                hintText: '*****',
                                //errorText: isValidPass ? null : _passError,
                                contentPadding: EdgeInsets.all(15.0),
                                isDense: true,
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.transparent, width: 0.0),
                                ),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  )),
              Visibility(
                  visible: isValidPass ? false : true,
                  child: Container(
                      padding: const EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
                      child: Text(
                        '$_passError',
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                            color: Colors.red,
                            fontSize: 12,
                            fontWeight: FontWeight.w400),
                      ))),
            ]));
  }

  Widget button() {
    return Container(
      height: 55,
      width: double.infinity,
      margin: const EdgeInsets.fromLTRB(25.0, 0.0, 25.0, 0.0),
      decoration: BoxDecoration(
          color: Constants.blue, borderRadius: BorderRadius.circular(25)),
      child: FlatButton(
        onPressed: () {
          validate();
          // Navigator.push(
          //     context, MaterialPageRoute(builder: (_) => const OrderPage()));
        },
        child: const Text(
          Constants.login,
          style: TextStyle(color: Colors.white, fontSize: 18),
        ),
      ),
    );
  }

  void validate() {
    setState(() {
      if (_numberController.text.isEmpty ||
          _numberController.text.length < 10) {
        _numberError = Constants.error_mobile;
        isValidMobile = false;
      } else {
        isValidMobile = true;
      }

      if (_passwordController.text.isEmpty) {
        _passError = Constants.error_password;
        isValidPass = false;
      } else {
        isValidPass = true;
      }
    });
    if (isValidMobile && isValidPass) {
      setState(() {
        isProgress = true;
      });
      getLogin(_numberController.text, _passwordController.text);
    }
    //showSnackBar("Login Successfully");
  }

  getLogin(String user, String pass) async {
    pref = await SharedPreferences.getInstance();
    Map data = {'email': user, 'password': pass};
    try {
      http
          .post(Uri.parse(collection.endpoint_login), body: data)
          .then((response) {
        setState(() {
          isProgress = false;
        });
        if (response.statusCode == 200) {
          print("------>>>>>: ${response.body}");
          UserModel model = UserModel.fromJson(jsonDecode(response.body));
          pref.setString(PrefStore.usertype, model.results.user.user_type);
          pref.setString(PrefStore.token, model.results.access_token);
          showSnackBar(Constants.msg_login_success);
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (_) => const History()),
              ModalRoute.withName("/history"));
        } else {
          print("------>>>>>: ${response.body}");
          ErrorModel model = ErrorModel.fromJson(jsonDecode(response.body));
          showSnackBar(model.message);
        }
      });
    } catch (Exc) {
      setState(() {
        isProgress = false;
      });
      print(Exc);
      rethrow;
    }
  }

  Future<void> checkIsLogin() async {
    pref = await SharedPreferences.getInstance();
    if (pref.getString(PrefStore.token) != null) {
      print("------>>>>>: ${pref.getString(PrefStore.token)}");
      //Navigator.push(
      //context, MaterialPageRoute(builder: (_) => const OrderPage()));
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => const History()),
          ModalRoute.withName("/history"));
    }
  }

  void showSnackBar(String s) {
    /*final snackBar = SnackBar(
      content: Text(s),
      duration: const Duration(seconds: 3),
      backgroundColor: Theme.of(context).primaryColor,
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);*/

    Fluttertoast.showToast(
        msg: s,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1);
  }
}
