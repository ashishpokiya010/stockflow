import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stockflow/api/errorModel.dart';
import 'package:stockflow/api/loginApi.dart';
import 'package:stockflow/preference/preferenceStore.dart';
import 'package:stockflow/screen/history.dart';
import 'package:stockflow/screen/login.dart';
import 'package:stockflow/utils/Cuvedshape.dart';
import 'package:stockflow/utils/constants.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'order.dart';
import 'package:stockflow/api/collection.dart';

class Splash extends StatefulWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  late SharedPreferences pref;

  bool isLogin = false;

  @override
  void initState() {
    super.initState();
    checkIsLogin();
    Timer(
        const Duration(seconds: 2),
        () => Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    isLogin ? const History() : const Login())));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: Constants.blue,
      child: Center(
        child: Container(
          height: 210,
          width: 210,
          alignment: Alignment.center,
          /*decoration: const BoxDecoration(
            color: Colors.white,
            shape: BoxShape.circle,
            boxShadow: [
              BoxShadow(
                color: Colors.white,
                blurRadius: 10.0,
                offset: Offset(0.0, 10.0),
              ),
            ],
          ),*/
          child:Container(
            decoration: const BoxDecoration(
                image: DecorationImage(
                    image:
                    AssetImage('assets/images/stockflow_logo.png'),
                  fit: BoxFit.fill,),),
          ),
        ),
      ),
    );
  }

  Future<void> checkIsLogin() async {
    pref = await SharedPreferences.getInstance();
    if (pref.getString(PrefStore.token) != null) {
      print("------>>>>>: ${pref.getString(PrefStore.token)}");
      setState(() {
        isLogin = true;
      });
    }
  }
}
