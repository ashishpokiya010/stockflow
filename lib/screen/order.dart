import 'package:flutter/material.dart';
import 'package:stockflow/dialog/add_dialog.dart';
import 'package:stockflow/dialog/logout_dialog.dart';
import 'package:stockflow/dialog/product_dialog.dart';
import 'package:stockflow/utils/constants.dart';

class OrderPage extends StatefulWidget {
  const OrderPage({Key? key}) : super(key: key);

  @override
  _OrderPageState createState() => _OrderPageState();
}

class _OrderPageState extends State<OrderPage> {
  late int tappedIndex;

  @override
  void initState() {
    super.initState();
    tappedIndex = -1;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: appbarModel(),
        body: Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Expanded(
                child: SingleChildScrollView(
                  child: ListView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: 5,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          child: ListTile(
                              title: cardView(index),
                              //title: selectedCardView(index),
                              onTap: () {
                                setState(() {
                                  if (tappedIndex == index) {
                                    tappedIndex = -1;
                                  } else {
                                    tappedIndex = index;
                                  }
                                });
                              }),
                        );
                      }),
                ),
              ),
              bottomView(),
            ],
          ),
        ),
      ),
    );
  }

  appbarModel() {
    return AppBar(
      toolbarHeight: 75,
      backgroundColor: Constants.blue,
      title: const Text(Constants.stockflow),
      actions: <Widget>[
        Padding(
            padding: const EdgeInsets.only(right: 20.0),
            child: GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return const LogoutDialog();
                    });
              },
              child: const Icon(
                Icons.assignment_ind_outlined,
                size: 26.0,
              ),
            )),
      ],
    );
  }

  bottomView() {
    return Container(
        padding: const EdgeInsets.all(5.0),
        margin: const EdgeInsets.all(5.0),
        child: Row(
          children: <Widget>[
            Expanded(
              child: GestureDetector(
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return const ProductDialog([]);
                      });
                },
                child: Container(
                  height: 55,
                  width: double.infinity,
                  margin: const EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(15)),
                  child: Container(
                      child: Row(children: <Widget>[
                    Expanded(
                      child: Container(
                        padding:
                            const EdgeInsets.fromLTRB(20.0, 0.0, 10.0, 0.0),
                        child: const Text(
                          Constants.product,
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 15,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Container(
                        margin: const EdgeInsets.fromLTRB(5.0, 0.0, 20.0, 0.0),
                        child: const IconTheme(
                          data: IconThemeData(color: Colors.black),
                          child: Icon(Icons.arrow_forward),
                        ),
                      ),
                    ),
                  ])),
                ),
              ),
            ),
            (tappedIndex != -1) ? checkView() : plusMinusView(),
          ],
        ));
  }

  Widget cardView(int index) {
    int tt = ((index % 2) == 0) ? 0 : 1;
    //Color changes by selected view;
    Color textColor = (tappedIndex == index) ? Colors.white : Colors.black;
    Color dateColor = (tappedIndex == index) ? Colors.white : Colors.grey;
    Color iconColor = (tappedIndex == index) ? Constants.blue : Colors.white;
    Color iconShapeColor =
        (tappedIndex == index) ? Colors.white : Constants.blue;
    return (tappedIndex == index)
        ? selectedCardView(index)
        : Container(
            padding: tt == 0
                ? const EdgeInsets.fromLTRB(10.0, 10.0, 45.0, 5.0)
                : const EdgeInsets.fromLTRB(45.0, 10.0, 10.0, 5.0),
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              color: tappedIndex == index ? Constants.blue : Colors.grey[300],
              elevation: 2,
              child: Container(
                padding: const EdgeInsets.fromLTRB(10.0, 5.0, 10, 5.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              padding: const EdgeInsets.all(5.0),
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    SizedBox(
                                      width: double.infinity,
                                      child: Text(
                                        Constants.product,
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          color: textColor,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: double.infinity,
                                      child: Text(
                                        "6204 NRB",
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            color: textColor,
                                            fontSize: 15,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    )
                                  ])),
                        ),
                        Expanded(
                          child: Container(
                              padding: EdgeInsets.all(5.0),
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    SizedBox(
                                      width: double.infinity,
                                      child: Text(
                                        Constants.quantity_added,
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                          color: textColor,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: double.infinity,
                                      child: Text(
                                        "25",
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                            color: textColor,
                                            fontSize: 15,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ])),
                        ),
                      ],
                    ),
                    Container(
                      padding: const EdgeInsets.all(5.0),
                      margin: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                      width: double.infinity,
                      child: Text(
                        Constants.remarks,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: textColor,
                          fontSize: 14,
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
                      margin: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                      width: double.infinity,
                      child: Text(
                        "delivery challan 125A Mahesh Bearings",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: textColor,
                          fontSize: 14,
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                      margin: const EdgeInsets.fromLTRB(5.0, 15.0, 5.0, 5.0),
                      width: double.infinity,
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              '01 Dec 2021,10:05 PM',
                              style: TextStyle(
                                  color: dateColor,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w300),
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomRight,
                            child: Container(
                              height: 30,
                              width: 30,
                              margin:
                                  const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                              decoration: BoxDecoration(
                                  color: iconShapeColor,
                                  borderRadius: BorderRadius.circular(25)),
                              child: IconTheme(
                                data: IconThemeData(color: iconColor),
                                child: tt == 0
                                    ? const Icon(Icons.add)
                                    : const Icon(Icons.remove),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
  }

  Widget selectedCardView(int index) {
    int tt = ((index % 2) == 0) ? 0 : 1;
    //Color changes by selected view;
    Color textColor = Colors.white;
    Color dateColor = Colors.white;
    Color iconColor = Constants.blue;
    Color iconShapeColor = Colors.white;
    Color cardColor = Constants.blue;
    return Container(
      padding: tt == 0
          ? const EdgeInsets.fromLTRB(10.0, 10.0, 45.0, 5.0)
          : const EdgeInsets.fromLTRB(45.0, 10.0, 10.0, 5.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        color: cardColor,
        elevation: 2,
        child: Container(
          padding: const EdgeInsets.fromLTRB(10.0, 5.0, 10, 5.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                        padding: const EdgeInsets.all(5.0),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              SizedBox(
                                width: double.infinity,
                                child: Text(
                                  Constants.product,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    color: textColor,
                                    fontSize: 14,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: double.infinity,
                                child: Text(
                                  "6204 NRB",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: textColor,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w500),
                                ),
                              )
                            ])),
                  ),
                  Expanded(
                    child: Container(
                        padding: EdgeInsets.all(5.0),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              SizedBox(
                                width: double.infinity,
                                child: Text(
                                  Constants.total_stock,
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                    color: textColor,
                                    fontSize: 14,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: double.infinity,
                                child: Text(
                                  "25",
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                      color: textColor,
                                      fontSize: 35,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ])),
                  ),
                ],
              ),
              Container(
                height: 40,
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
                margin: const EdgeInsets.fromLTRB(5.0, 15.0, 5.0, 5.0),
                width: double.infinity,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        '01 Dec 2021,10:05 PM',
                        style: TextStyle(
                            color: dateColor,
                            fontSize: 12,
                            fontWeight: FontWeight.w300),
                      ),
                    ),
                    Container(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget plusMinusView() {
    return Container(
        height: 55,
        width: 120,
        margin: const EdgeInsets.fromLTRB(5.0, 0.0, 10.0, 0.0),
        decoration: BoxDecoration(
            color: Colors.transparent, borderRadius: BorderRadius.circular(15)),
        child: Row(children: <Widget>[
          GestureDetector(
            onTap: () {
              showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (BuildContext context) {
                    return const AddDialog(type: 'remove');
                  });
            },
            child: Container(
              height: 55,
              width: 55,
              margin: const EdgeInsets.fromLTRB(0.0, 0.0, 5.0, 0.0),
              decoration: BoxDecoration(
                  color: Constants.blue,
                  borderRadius: BorderRadius.circular(15)),
              child: const IconTheme(
                data: IconThemeData(color: Colors.white),
                child: Icon(Icons.remove),
              ),
            ),
          ),
          GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return const AddDialog(type:"add");
                    });
              },
              child: Container(
                height: 55,
                width: 55,
                margin: const EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
                decoration: BoxDecoration(
                    color: Constants.blue,
                    borderRadius: BorderRadius.circular(15)),
                child: const IconTheme(
                  data: IconThemeData(color: Colors.white),
                  child: Icon(Icons.add),
                ),
              )),
        ]));
  }

  Widget checkView() {
    return Container(
      height: 55,
      width: 120,
      padding: const EdgeInsets.all(10.0),
      margin: const EdgeInsets.fromLTRB(5.0, 0.0, 10.0, 0.0),
      decoration: BoxDecoration(
          color: Constants.blue, borderRadius: BorderRadius.circular(15)),
      child: GestureDetector(
        onTap: () {
          showDialog(
              context: context,
              barrierDismissible: false,
              builder: (BuildContext context) {
                return const AddDialog();
              });
        },
        child: Container(
          child: const Center(
            child: Text(
              Constants.check,
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                  fontWeight: FontWeight.w400),
            ),
          ),
        ),
      ),
    );
  }
}
