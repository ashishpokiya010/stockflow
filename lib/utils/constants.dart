import 'package:flutter/material.dart';

class Constants {
  //static Color blue = const Color(0xFF2962FF) ;
  static Color blue = const Color(0xFF2B5FE9) ;
  static Color yellow_white = const Color(0xFFEEEEEB) ;
  static Color yellow = const Color(0xFFFFD400) ;



  static const String appName="Stockflow";
  static const String stockflow="Stockflow";
  static const String login_disc="Login with your mobile number and PIN";
  static const String pin="PIN";
  static const String mobile_number="Mobile Number";
  static const String login="Log in";
  static const String orders="Orders";
  static const String check="Check";
  static const String remarks="Remarks";
  static const String quantity_added="Quantity Added";
  static const String quantity_remove="Quantity Minus";
  static const String total_stock="Total Stock";
  static const String product="Product";
  static const String select_product="Select Product";
  static const String quantity="Quantity";
  static const String logout="Logout";
  static const String logout_msg="Are you sure you want to logout?";
  static const String yes="Yes";
  static const String no="No";
  static const String remove="remove";
  static const String add="add";
  static const String Added="Added";
  static const String Minus="Minus";


  static const String opening_balance="Opening Balance";
  static const String recent_update="Recent Updates";

  static const String error_qty="Please enter a Quantity";
  static const String error_remarks="Please enter a remarks";
  static const String error_mobile="Please enter your valid mobile number";
  static const String error_password="Please enter your valid password";


  static const String msg_login_success="Login successfull";
  static const String msg_product_selection="Please select product";
  static const String msg_no_product_history="No product history found";
  static const String msg_no_product="No product found";
  static const String msg_no_product_available="No product available";



}
