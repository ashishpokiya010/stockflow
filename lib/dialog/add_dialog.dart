import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stockflow/api/errorModel.dart';
import 'package:stockflow/preference/preferenceStore.dart';
import 'package:stockflow/screen/login.dart';
import 'package:stockflow/utils/constants.dart';
import 'package:http/http.dart' as http;
import 'package:stockflow/api/collection.dart';

class AddDialog extends StatefulWidget {
  final String? type;
  final String? productId;

  const AddDialog({Key? key, this.type, this.productId}) : super(key: key);

  @override
  _AddDialogState createState() => _AddDialogState();
}

class _AddDialogState extends State<AddDialog> {
  late TextEditingController _qController;

  late TextEditingController _rController;

  bool isValidate = true;
  bool isValidateQty = true;
  bool isValidateRemark = true;
  String errorQ = '';
  String errorR = '';

  @override
  void initState() {
    super.initState();
    _qController = TextEditingController();
    _rController = TextEditingController();
  }

  @override
  void dispose() {
    _qController.dispose();
    _rController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context, widget.type, widget.productId),
    );
  }

  dialogContent(BuildContext context, String? type, String? productId) {
    return Container(
      padding: const EdgeInsets.fromLTRB(20.0, 25.0, 20.0, 15.0),
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(15),
        boxShadow: const [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: Offset(0.0, 10.0),
          ),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          const SizedBox(
            width: double.infinity,
            child: Text(
              Constants.quantity,
              textAlign: TextAlign.start,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 14,
                  fontWeight: FontWeight.w400),
            ),
          ),
          Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
                  decoration: BoxDecoration(
                      border: Border.all(
                        width: 0.3,
                        color: Colors.grey,
                      ),
                      borderRadius: BorderRadius.circular(2)),
                  child: TextField(
                    controller: _qController,
                    keyboardType: TextInputType.number,
                    onChanged: (inputValue) {
                      setState(() {
                        if (inputValue.isEmpty) {
                          isValidateQty = false;
                        } else {
                          isValidateQty = true;
                        }
                      });
                    },
                    decoration: const InputDecoration(
                      hintText: Constants.quantity,
                      contentPadding: EdgeInsets.all(15.0),
                      isDense: true,
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                        BorderSide(color: Colors.transparent, width: 0.0),
                      ),
                      border: InputBorder.none,
                    ),
                  ),
                ),
                Visibility(
                    visible: isValidateQty ? false : true,
                    child: Container(
                        padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
                        child: const Text(
                          Constants.error_qty,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.red,
                              fontSize: 12,
                              fontWeight: FontWeight.w400),
                        ))),
              ]),
          const SizedBox(
            width: double.infinity,
            child: Text(
              Constants.remarks,
              textAlign: TextAlign.start,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 14,
                  fontWeight: FontWeight.w400),
            ),
          ),
          Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
                  decoration: BoxDecoration(
                      border: Border.all(
                        width: 0.3,
                        color: Colors.grey,
                      ),
                      borderRadius: BorderRadius.circular(2)),
                  child: TextField(
                    controller: _rController,
                    onChanged: (inputValue) {
                      setState(() {
                        if (inputValue.isEmpty) {
                          isValidateRemark = false;
                        } else {
                          isValidateRemark = true;
                        }
                      });
                    },
                    keyboardType: TextInputType.multiline,
                    decoration: const InputDecoration(
                      hintText: Constants.remarks,
                      contentPadding: EdgeInsets.all(15.0),
                      isDense: true,
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                        BorderSide(color: Colors.transparent, width: 0.0),
                      ),
                      border: InputBorder.none,
                    ),
                    maxLines: 5,
                    minLines: 4,
                  ),
                ),
                Visibility(
                    visible: isValidateRemark ? false : true,
                    child: Container(
                        padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
                        child: const Text(
                          Constants.error_remarks,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.red,
                              fontSize: 12,
                              fontWeight: FontWeight.w400),
                        ))),
              ]),
          GestureDetector(
            onTap: () {
              //Navigator.of(context).pop();
              checkValidate(type, productId);
            },
            child: Container(
              height: 60,
              width: 60,
              margin: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 15.0),
              decoration: BoxDecoration(
                  color: Constants.blue,
                  borderRadius: BorderRadius.circular(100)),
              child: IconTheme(
                data: const IconThemeData(color: Colors.white),
                child: (type == Constants.add)
                    ? const Icon(Icons.add)
                    : const Icon(Icons.remove),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void setValidator(valid) {
    setState(() {
      isValidate = valid;
    });
  }

  void checkValidate(String? type, String? pId) {
    setState(() {
      _qController.text.isEmpty ? isValidateQty = false : isValidateQty = true;
      _rController.text.isEmpty
          ? isValidateRemark = false
          : isValidateRemark = true;
    });
    if (isValidateQty && isValidateRemark) {
      print('id-->>${pId}');
      print('qty-->>${_qController.text}');
      print('rmk-->>${_rController.text}');
      String update_type =
      (type == Constants.add ? Constants.Added : Constants.Minus);
      getProductUpdate(update_type, pId, _qController.text, _rController.text);
    }
  }

  Future<void> getProductUpdate(
      String type, String? pId, String qty, String rms) async {
    String apiEndPoint = '';
    SharedPreferences pref = await SharedPreferences.getInstance();

    String token = pref.getString(PrefStore.token);
    apiEndPoint = collection.endpoint_product_update;
    Map data = {
      'product_id': pId,
      'update_type': type,
      'update_quantity': qty,
      'remark': rms,
    };
    try {
      http
          .post(Uri.parse(apiEndPoint),
          headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer $token',
          },
          body: data)
          .then((response) {
        print("update------>>>>>: ${response.statusCode}");
        if (response.statusCode == 201) {
          print("update------>>>>>: ${response.body}");
          ErrorModel model = ErrorModel.fromJson(jsonDecode(response.body));
          showSnackBar(model.message);
          Navigator.pop(context, pId);
        } else if (response.statusCode == 401) {
          ErrorModel model = ErrorModel.fromJson(jsonDecode(response.body));
          showSnackBar(model.message);
          Navigator.of(context).pop();
          logout();
        } else {
          ErrorModel model = ErrorModel.fromJson(jsonDecode(response.body));
          showSnackBar(model.message);
          Navigator.of(context).pop();
        }
      });
    } catch (Exc) {
      print(Exc);
      rethrow;
    }
  }

  logout() {
    PrefStore.remove();
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => const Login()),
        ModalRoute.withName("/login"));
  }

  void showSnackBar(String s) {
    Fluttertoast.showToast(
        msg: s,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1);
  }
}
