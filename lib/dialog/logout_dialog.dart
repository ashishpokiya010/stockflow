import 'package:flutter/material.dart';
import 'package:stockflow/preference/preferenceStore.dart';
import 'package:stockflow/screen/login.dart';
import 'package:stockflow/utils/constants.dart';

class LogoutDialog extends StatefulWidget {
  final String? type;

  const LogoutDialog({Key? key, this.type}) : super(key: key);

  @override
  _LogoutDialogState createState() => _LogoutDialogState();
}

class _LogoutDialogState extends State<LogoutDialog> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context, widget.type),
    );
  }

  dialogContent(BuildContext context, String? type) {
    return Container(
      padding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(15),
        boxShadow: const [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: Offset(0.0, 10.0),
          ),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          const Text(
            Constants.logout,
            textAlign: TextAlign.start,
            style: TextStyle(
                color: Colors.black, fontSize: 20, fontWeight: FontWeight.w700),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 5.0),
            child: const Text(
              Constants.logout_msg,
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 17,
                  fontWeight: FontWeight.w400),
            ),
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Container(
                width: 80,
                margin: const EdgeInsets.fromLTRB(0.0, 20.0, 15.0, 5.0),
                padding: const EdgeInsets.fromLTRB(25.0, 8.0, 25.0, 8.0),
                decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(30)),
                child: const Text(
                  Constants.no,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 15,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                //Navigator.push(context, MaterialPageRoute(builder: (_) => const Login()));
                Navigator.of(context).pop();
                PrefStore.remove();
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const Login()
                    ),
                    ModalRoute.withName("/login")
                );
              },
              child: Container(
                width: 80,
                margin: const EdgeInsets.fromLTRB(0.0, 20.0, 5.0, 5.0),
                padding: const EdgeInsets.fromLTRB(25.0, 8.0, 25.0, 8.0),
                decoration: BoxDecoration(
                    color: Constants.blue,
                    borderRadius: BorderRadius.circular(30)),
                child: const Text(
                  Constants.yes,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 15,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
          ]),
        ],
      ),
    );
  }
}
