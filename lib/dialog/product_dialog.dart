import 'package:flutter/material.dart';
import 'package:stockflow/model/product.dart';
import 'package:stockflow/utils/constants.dart';

class ProductDialog extends StatefulWidget {
  final List<Product> products;

  const ProductDialog(this.products, {Key? key}) : super(key: key);

  @override
  _ProductDialogState createState() => _ProductDialogState();
}

class _ProductDialogState extends State<ProductDialog> {
  final _searchController = TextEditingController();
  late List<Product> search_Products = [];

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context, widget.products),
    );
  }

  dialogContent(BuildContext context, List<Product> products) {
    print('content ---- ${products.length}');
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(15),
        boxShadow: const [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: Offset(0.0, 10.0),
          ),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            height: 55,
            width: double.infinity,
            margin: const EdgeInsets.fromLTRB(15.0, 20.0, 15.0, 0.0),
            decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(30)),
            child: Container(
                child: Row(children: <Widget>[
              Expanded(
                child: GestureDetector(
                  onTap: () {},
                  child: Container(
                      padding: const EdgeInsets.fromLTRB(20.0, 0.0, 10.0, 0.0),
                      /*child: const Text(
                      Constants.select_product,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 15,
                          fontWeight: FontWeight.w400),
                    ),*/
                      child: TextFormField(
                        controller: _searchController,
                        onChanged: (value) {
                          setState(() {
                            if (value.isEmpty) {
                              search_Products.clear();
                            } else {
                              search_Products = getPro(value, products);
                            }
                          });
                        },
                        keyboardType: TextInputType.text,
                        decoration: const InputDecoration(
                          hintText: Constants.select_product,
                          //errorText: isValidMobile ? null : _numberError,
                          contentPadding: EdgeInsets.all(15.0),
                          isDense: true,
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.transparent, width: 0.0),
                          ),
                          border: InputBorder.none,
                        ),
                      )),
                ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Container(
                  margin: const EdgeInsets.fromLTRB(5.0, 0.0, 20.0, 0.0),
                  child: const IconTheme(
                    data: IconThemeData(color: Colors.black),
                    child: Icon(Icons.arrow_downward),
                  ),
                ),
              ),
            ])),
          ),
          Flexible(
            child: Container(
              width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 15.0),
                decoration: BoxDecoration(
                    border: Border.all(
                      width: 0.3,
                      color: Colors.grey,
                    ),
                    borderRadius: BorderRadius.circular(15)),
                child: SingleChildScrollView(
                  child:(_searchController.text.isEmpty)
                      ? ListView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      itemCount: products.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {
                            Navigator.pop(context, products[index]);
                          },
                          child: Container(
                              margin: const EdgeInsets.fromLTRB(
                                  15.0, 10.0, 15.0, 10.0),
                              child: Text(
                                products[index].product_name.toString(),
                                textAlign: TextAlign.start,
                                style: const TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                ),
                              )),
                        );
                      })
                      : (search_Products.isEmpty)
                      ?Container(
                          margin: const EdgeInsets.fromLTRB(
                              15.0, 10.0, 15.0, 10.0),
                          child: const Text(
                            Constants.msg_no_product,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                            ),
                          ))
                      :ListView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      itemCount: search_Products.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {
                            Navigator.pop(context, search_Products[index]);
                          },
                          child: Container(
                              margin: const EdgeInsets.fromLTRB(
                                  15.0, 10.0, 15.0, 10.0),
                              child: Text(
                                search_Products[index].product_name.toString(),
                                textAlign: TextAlign.start,
                                style: const TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                ),
                              )),
                        );
                      }),
                )),
          )
        ],
      ),
    );
  }

  List<Product> getPro(String value, List<Product> products) {
    List<Product> list =
        products.where((p) => p.product_name.contains(value)).toList();

    return list;
  }
}
