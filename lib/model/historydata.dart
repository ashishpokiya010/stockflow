import 'dart:convert';

import 'package:stockflow/api/loginApi.dart';
import 'package:stockflow/model/product.dart';

class HistoryModel {
  final int status;
  final String message;
  final List<HistoryData> results;

  HistoryModel({
    required this.status,
    required this.message,
    required this.results,
  });

  factory HistoryModel.fromJson(Map<String, dynamic> json) {
    return HistoryModel(
      status: json['status'],
      message: json['message'],
      results: (json['results']!=null)?(json['results']as List).map((e) => HistoryData.fromJson(e)).toList():[],
    );
  }
}
class HistoryData {
  int id;
  int product_id;
  int user_id;
  String update_type;
  int update_quantity;
  int final_quantity;
  String remark;
  String created_at;
  String updated_at;
  String deleted_at;
  Product product;
  UserData users;

  HistoryData({
    required this.id,
    required this.product_id,
    required this.user_id,
    required this.update_type,
    required this.update_quantity,
    required this.final_quantity,
    required this.remark,
    required this.created_at,
    required this.updated_at,
    required this.deleted_at,
    required this.product,
    required this.users,
  });


  factory HistoryData.fromJson(Map<String, dynamic> json) {
    return HistoryData(
        id: json['id'],
      product_id: json['product_id'],
      user_id: json['user_id'],
      update_type: json['update_type'],
      update_quantity: json['update_quantity'],
      final_quantity: json['final_quantity'],
      remark: json['remark'],
      created_at: json['created_at'],
      updated_at: json['updated_at'],
      deleted_at: json['deleted_at'],
      product: Product.fromJson(json['products']),
      users: UserData.fromJson(json['users']),
    );
  }
}
/*
class Product {
  int id;
  String product_name;
  int product_quantity;
  String created_at;
  String updated_at;
  String deleted_at;

  Product({
    required this.id,
    required this.product_name,
    required this.product_quantity,
    required this.created_at,
    required this.updated_at,
    required this.deleted_at
  });


  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
        id: json['id'],
        product_name: json['product_name'],
        product_quantity: json['product_quantity'],
        created_at: json['created_at'],
        updated_at: json['updated_at'],
        deleted_at: json['deleted_at']
    );
  }
}
*/
