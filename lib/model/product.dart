import 'dart:convert';

class ProductModel {
  final int status;
  final String message;
  final List<Product> results;

  ProductModel({
    required this.status,
    required this.message,
    required this.results,
  });

  factory ProductModel.fromJson(Map<String, dynamic> json) {
    return ProductModel(
      status: json['status'],
      message: json['message'],
      results: (json['results']!=null)?(json['results']as List).map((e) => Product.fromJson(e)).toList():[],
    );
  }
}
class Product {
  int id;
  String product_name;
  int product_quantity;
  String created_at;
  String updated_at;
  String deleted_at;

  Product({
    required this.id,
    required this.product_name,
    required this.product_quantity,
    required this.created_at,
    required this.updated_at,
    required this.deleted_at
  });


  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
      id: json['id'],
      product_name: json['product_name'],
      product_quantity: json['product_quantity'],
      created_at: json['created_at'],
      updated_at: json['updated_at'],
      deleted_at: json['deleted_at']
    );
  }
}
